---
title: Train Travel
---
If you want to go anywhere in Japan, you're going to be riding the train.

![](./7/train.jpg)

![](./7/city.jpg)

![](./7/river.jpg)

![](./7/farm.jpg)

A brief wait in Toyama.

![](./7/toyama1.jpg)

Extreme crowds in Toyama.

![](./7/toyama2.jpg)

The station in Takayama only opened when the trains showed up.

![](./7/early.jpg)

Half of a train.

![](./7/trainhookup.jpg)

Some very well-behaved schoolkids at a train station.

![](./7/kiddos.jpg)

Critical toilet instructions for train toilets.

![](./7/instructions.jpg)

![](./7/mountain.jpg)

![](./7/water.jpg)

A fancy decorated train car in Kyoto.

![](./7/kyotocar.jpg)

Hankyu rail station in Kyoto.

![](./7/hankyu.jpg)

![](./7/sky1.jpg)

![](./7/sky2.jpg)

Just like me, this guy was taking train pictures.

![](./7/myhero.jpg)


Sightseeing in [Kyoto](./8-kyoto).
