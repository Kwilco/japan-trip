---
title: Shopping
---

View from the terrifying outdoor stairway on the side of Mandarake, a second-hand manga/anime store.

![](./3/mandarake.jpg)

Get your Freddie Mercury figurines here!

![](./3/freddie.jpg)

Yodobashi Camera is essentially 8 department stores stacked on top of each other. It might be the
best store ever made.

Literally thousands of phone cases.

![](./3/cases.jpg)

Speaking of phones, every single one of these several hundred land-line phones on display was
ringing non-stop. I can only imagine the pain and suffering of working on this floor.

![](./3/phones.jpg)

![](./3/watch.jpg)

![](./3/keyboard.jpg)

![](./3/headphones.jpg)

Anime sounds better when you listen to it with Anime Earbuds!

![](./3/animeearbuds.jpg)

This strange speaker was blasting Michael Jackson's "Beat It".

![](./3/mj.jpg)

Awesome Dark Souls statue.

![](./3/darksouls.jpg)

![](./3/warhammer.jpg)

![](./3/rabite.jpg)

A lot of sweet anime figurines in one case.

![](./3/figs.jpg)

Hundreds of Gundams...

![](./3/gundam.jpg)

![](./3/gundam2.jpg)

![](./3/gundam3.jpg)

![](./3/gundam4.jpg)

Some cool suitcases. Not as cool as mine covered in Pokemon stickers though.

![](./3/suitcases.jpg)

I'm glad to know that my AV receiver brand has cute anime mascots.

![](./3/denon.jpg)

I saw at least a dozen people watching this movie on the plane.

![](./3/psychic.jpg)

I think this is a better name than **Furious 7**.

![](./3/skymission.jpg)

A climate-controlled guitar room?

![](./3/guitar.jpg)

Creepy dancing Star Wars figurines.

![](./3/starwars.jpg)

My loot. Wouldn't you like to know?!

![](./3/loot.jpg)


[Hungry](./4-food)?
