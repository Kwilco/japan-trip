---
title: Food
---

I love Japanese food. I tried everything put in front of me, and all of it was good. Even the
enoki mushrooms in my ramen were edible.

Absolutely incredible ramen. Broth so fatty and flavorful that I could feel my arteries clogging as I ate.

![](./4/ramen1.jpg)

![](./4/ramen2.jpg)

Teriyaki burger at MOS Burger.

![](./4/mosburger.jpg)

Some weird chicken and egg thing from McDonald's. I ordered the strangest thing on the menu.

![](./4/mcd.jpg)

Incredible sushi.

![](./4/sushi.jpg)

I may have eaten a lot.

![](./4/plates.jpg)

Lots of good street food.

Kobe beef skewer. Tasty, but underwhelming for the price.

![](./4/kobe.jpg)

Super Princess Crepe. ¥300 more than the Princess Crepe.

![](./4/crepe.jpg)

Some Dango.

![](./4/dango.jpg)

Every hotel I stayed at had beer in the vending machines. A brilliant concept.

![](./4/beer.jpg)

Japanese pizza place that has successfully emulated crappy American pizza.

![](./4/pizza.jpg)

![](./4/pizzachips.jpg)

Pretty good sandwiches at the 7/11.

![](./4/sammy.jpg)

![](./4/caloriemate.jpg)

I waited in line for 2 hours to eat at this Gyūkatsu place. Amazing, but not worth the wait.

![](./4/gyukatsu1.jpg)

I had to grill the meat myself.

![](./4/gyukatsu2.jpg)

No idea what this was. I bought it from a street vendor in Takayama.

![](./4/whoknows.jpg)

Hida beef (regional specialty of next-door town of Hida) "sushi". Absolutely incredible. Might be
the best thing I've ever eaten. This alone made the trip to Takayama worth it.

![](./4/hidasuhi.jpg)

Hida beef bowl. It was less blurry in real life.

![](./4/hidabeefbowl.jpg)

I don't know what I expected for ¥100.

![](./4/giantbeer.jpg)

Omurice in Kyoto. I couldn't read the menu so I ordered randomly. Nasty surprise to find it filled
with mushrooms.

![](./4/omurice.jpg)

Super delicious curry from the place next to my hotel in Kyoto. I ate there... a lot.

![](./4/curry.jpg)

Some green tea that had the consistency of lawn clippings, and some wierd thing filled with green
tea slime. They were both refreshing and delicious.

![](./4/tea.jpg)

Okonomiyaki. Looks like garbage, tastes amazing.

![](./4/oko.jpg)

Local shochu sampler at Miyajima. Amazing.

![](./4/shochu.jpg)

Bonito sashimi.

![](./4/bonito.jpg)

Deep fried oyster curry.

![](./4/oyster.jpg)

Quick noodles at Shin-Kobe station.

![](./4/noodles.jpg)


Time to hit the [arcade](./5-arcade).
