---
title: Miyajima
---
There are two separate ferry systems competing to take you to the island. Either keeping the
price low through competition or working together to fix the price high. Thankfully I didn't
have to find out. My rail pass covered the ferry run by JR.

![](./10/ferry1.jpg)

![](./10/ferry2.jpg)

Really beautiful island.

![](./10/afternoon.jpg)

Too bad it's overrun with giant rats.

![](./10/stiltrat.jpg)

Ugly, mangy stilt rats.

![](./10/infested.jpg)

Pretty cool hotel room.

![](./10/hotel.jpg)

Nice view of the ferry terminal from the room.

![](./10/hotelview.jpg)

The Itsukushima Shrine "floating gate" lit up at night.

![](./10/gatenight.jpg)

The gate at high tide.

![](./10/gate1.jpg)

![](./10/gate2.jpg)

Gorgeous view from the ferry back to the mainland.

![](./10/ferryview1.jpg)

![](./10/ferryview2.jpg)

![](./10/ferryview3.jpg)


An overall [summary](./11-summary) of the trip.
