---
title: Ueno Park
---

[Ueno Park](https://en.wikipedia.org/wiki/Ueno_Park) is a park in Tokyo that's pretty cool.

![](./6/map.jpg)

![](./6/egg.jpg)

A single cherry tree decided it was go time.

![](./6/cherry.jpg)


## Ueno Park Zoo

There's a pretty cool zoo in the park.

![](./6/penguins.jpg)

![](./6/flamingos.jpg)

![](./6/mole.jpg)

![](./6/tightrope.jpg)

I have no idea what this monstrosity is.

![](./6/monster.jpg)


## Bentendo Hall - [Link](http://www.asiarediscovery.com/japan/117-tokyo-ueno-park-bentendo-benten-hall-temple-a-benzaiten)

A pretty cool Bhuddist temple. Didn't get any good pictures of the temple itself, but the
surroundings were interesting.

![](./6/stone1.jpg)

![](./6/stone2.jpg)

![](./6/stone3.jpg)

![](./6/messageboard.jpg)

![](./6/papermessages.jpg)


Leaving Tokyo on a [train](./7-train).
