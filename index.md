---
title: Japan trip
---

![Welcome to Japan](./0/japan.jpg)

This is a writeup of my February 2018 trip to Japan, thanks for stopping by.
I took about 600 pictures. These are some of the better ones.

If you have any corrections, please do one of the following:

* Let me know the page.
* [Open an issue](https://gitlab.com/Kwilco/japan-trip/issues).
* Submit a Merge Request to fix it in the [source](https://gitlab.com/Kwilco/japan-trip).

Step one: [Getting to Japan](./1-to-tokyo).
