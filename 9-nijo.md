---
title: Nijo-jo
---
Welcome to Nijo castle.

![](./9/moat1.jpg)

![](./9/entrance.jpg)

![](./9/cornertower.jpg)

![](./9/gate.jpg)

Photos were prohibited in the palace proper. I snuck this one of the landscaping.

![](./9/forbidden.jpg)

![](./9/building.jpg)

![](./9/pond1.jpg)

![](./9/pond2.jpg)

Some strange covering for plants.

![](./9/wat.jpg)

![](./9/map.jpg)

Two moats are better than one.

![](./9/moat2.jpg)

![](./9/view.jpg)

![](./9/moat3.jpg)

Kids with matching hats.

![](./9/hats.jpg)

A big 'ol fortified gate.

![](./9/fortifiedgate.jpg)


Onwards to [Miyajima](./10-miyajima).
