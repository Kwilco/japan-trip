---
title: Akihabara
---

I stayed in [Akihabara](https://en.wikipedia.org/wiki/Akihabara), anime nerd capitol of the world.

![Akiba streets](./2/streets.jpg)

Three seconds left until the doors open! I'm about 70% sure these people were waiting to get into
an [AKB48](https://en.wikipedia.org/wiki/AKB48) show. Getting tickets is supposedly impossible.

![AKB48](./2/akb48.jpg)

The line wrapped around the block.

![More line](./2/akb48line.jpg)

Ehh, let's not...

![Maid cafe](./2/maid.jpg)

A neat-looking underpass:

![Underpass](./2/underpass.jpg)

Get your anime here!

![Anime](./2/kono.jpg)

Apparantly Mario Kart is based on real life.

![Mario kart](./2/kart.jpg)

![These assholes again?](./2/kart2.jpg)

Akihabara looks pretty awesome at night.

![Akiba nighttime](./2/night.jpg)

![More nighttime](./2/night2.jpg)

Some very imposing buildings hiding treasures...
![Yodobashi](./2/imposing.jpg)

[Shopping...](./3-shopping)
