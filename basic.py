#!/usr/bin/env python3.6

import sys
import os

dirname = sys.argv[-1]

for fn in os.listdir(dirname):
    print(f'![](./{dirname}/{fn})', end='\n\n')
