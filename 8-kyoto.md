---
title: Kyoto
---

Did a bunch of sightseeing in Kyoto.

![](./8/temple.jpg)

![](./8/tickets.jpg)

![](./8/mural.jpg)

![](./8/statue.jpg)

![](./8/bell.jpg)

![](./8/rocks.jpg)

![](./8/fish.jpg)

![](./8/pond.jpg)

![](./8/stone.jpg)

![](./8/bamboo.jpg)

Slightly out of frame: 700 people taking the same picture.

![](./8/bambooforest.jpg)

Some ladies in Kimono taking selfies.

![](./8/selfie1.jpg)

If I'm going to post stranger selfies, I guess I'm obligated to post one of myself.

![](./8/selfie2.jpg)

Had some strange but delicious tea in the middle of a garden tour.

![](./8/teahouse.jpg)

![](./8/steps.jpg)

![](./8/distance.jpg)

![](./8/hills.jpg)

![](./8/mountains.jpg)

A bunch of paths were blocked off for use by tour groups.

![](./8/forbidden.jpg)

![](./8/thing.jpg)

![](./8/riverstep.jpg)

![](./8/river.jpg)

![](./8/underbridge1.jpg)

![](./8/underbridge2.jpg)

![](./8/walkingbridge.jpg)

Once again, one single tree going early.

![](./8/cherry.jpg)

![](./8/night.jpg)

![](./8/people.jpg)


On to [Nijo-jo](./9-nijo).
